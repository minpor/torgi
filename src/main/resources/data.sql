TRUNCATE table organization CASCADE;
TRUNCATE table users CASCADE;
TRUNCATE table contract CASCADE;
TRUNCATE table trade CASCADE;

SELECT setval('public.contract_id_seq', 1);
SELECT setval('public.organization_id_seq', 1);
SELECT setval('public.trade_id_seq', 1);
SELECT setval('public.users_id_seq', 1);

INSERT INTO organization(name)
VALUES ('Имя 1'), ('Имя 2');

INSERT INTO users(login, name)
VALUES ('логин 1', 'имя 1'), ('логин 2', 'имя 2'), ('логин 3', 'имя 3'), ('логин 4', 'имя 4');

INSERT INTO organization_members(organization_id, members_id) VALUES (2, 2), (2, 3), (3, 4), (3, 5);

INSERT INTO contract(id)
VALUES (nextval('contract_id_seq'));

insert into contract_owners(contract_id, owners_id) VALUES (2,2), (2,4);

INSERT INTO trade(id)
VALUES (nextval('trade_id_seq'));

insert into trade_owners(trade_id, owners_id) VALUES (2,2), (2,5);