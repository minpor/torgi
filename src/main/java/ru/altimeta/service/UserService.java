package ru.altimeta.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import ru.altimeta.dao.*;
import ru.altimeta.domain.*;
import ru.altimeta.domain.enums.SubjectType;
import ru.altimeta.dto.CreateConversationDto;
import ru.altimeta.dto.MessageDto;
import ru.altimeta.dto.UserDto;
import ru.altimeta.exception.NotFoungException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

	@Autowired
	private ContractDao contractDao;
	@Autowired
	private TradeDao tradeDao;
	@Autowired
	private ConversationDao conversationDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private ChatDao chatDao;

	@Transactional(readOnly = true)
	public List<SubjectType> getSubjectTypes() {
		return new ArrayList<>(Arrays.asList(SubjectType.values()));
	}

	@Transactional(readOnly = true)
	public List<UserDto> getUserBySubjectType(int subjectType, Integer subjectId) throws NotFoungException {
		List<User> owners;
		if (subjectType == SubjectType.CONTRACT.getValue()) {
			Contract contract = contractDao.findById(subjectId);
			if (contract == null) {
				throw new NotFoungException("Не найден субъект contract");
			}

			owners = contract.getOwners();
			Assert.notNull(owners, "owners is null");
		} else if (subjectType == SubjectType.TRADE.getValue()) {
			Trade trade = tradeDao.findById(subjectId);
			if (trade == null) {
				throw new NotFoungException("Не найден субъект trade");
			}

			owners = trade.getOwners();
			Assert.notNull(owners, "owners is null");
		} else {
			// здесь мы ничего не делаем даже если у нас субъект не задан
			owners = new ArrayList<>();
		}

		return owners.stream().map(UserDto::new).collect(Collectors.toList());
	}

	@Transactional
	public void createConversation(CreateConversationDto dto) {
		Conversation conversation = new Conversation();
		int subjectType = dto.getSubjectType();

		if (subjectType == SubjectType.CONTRACT.getValue()) {
			//логика для субъекта Contract
			conversation.setType(SubjectType.CONTRACT);
			for (int i : dto.getUserId()) {
				User user = userDao.findById(i);
				conversation.getUser().add(user);
			}
		} else if (subjectType == SubjectType.TRADE.getValue()) {
			//логика для субъекта Trade
			conversation.setType(SubjectType.TRADE);
			for (int i : dto.getUserId()) {
				User user = userDao.findById(i);
				conversation.getUser().add(user);
			}
		} else {
			// логика для абстрактного диалога
			conversation.setType(SubjectType.NON_SUBJECT);
			for (int i : dto.getUserId()) {
				User user = userDao.findById(i);
				conversation.getUser().add(user);
			}
		}

		conversationDao.save(conversation);
	}

	@Transactional(readOnly = true)
	public List<Conversation> getConversations() {
		return conversationDao.findAll();
	}

	@Transactional(readOnly = true)
	public List<Chat> getMessages(long conversationId) throws NotFoungException {
		Conversation conversation = conversationDao.findById(conversationId);
		if (conversation == null) {
			throw new NotFoungException("Не найден субъект trade");
		}

		List<Chat> chat = chatDao.findByConversation(conversation);
		return chat;
	}

	@Transactional
	public void createMessage(MessageDto dto) throws NotFoungException {
		Conversation conversation = conversationDao.findById(dto.getConversationId());
		if (conversation == null) {
			throw new NotFoungException("Не найден субъект trade");
		}

		Chat chat = new Chat();
		chat.setConversation(conversation);
		chat.setMessage(dto.getMessage());

		User user = userDao.findById(dto.getUserId());
		if (user == null) {
			throw new NotFoungException("Не найден субъект trade");
		}
		chat.setUser(user);

		chatDao.save(chat);
	}
}
