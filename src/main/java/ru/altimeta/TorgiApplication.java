package ru.altimeta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TorgiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TorgiApplication.class, args);
	}
}
