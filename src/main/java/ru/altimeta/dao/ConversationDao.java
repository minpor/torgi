package ru.altimeta.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.altimeta.domain.Conversation;

public interface ConversationDao extends JpaRepository<Conversation, Long> {
	Conversation findById(long id);
}
