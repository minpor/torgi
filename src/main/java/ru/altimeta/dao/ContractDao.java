package ru.altimeta.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.altimeta.domain.Contract;

public interface ContractDao extends JpaRepository<Contract, Long> {
	Contract findById(Integer id);
}
