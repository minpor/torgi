package ru.altimeta.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.altimeta.domain.Organization;

public interface OrganizationDao extends JpaRepository<Organization, Long> {
}
