package ru.altimeta.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.altimeta.domain.Contract;
import ru.altimeta.domain.Trade;

public interface TradeDao extends JpaRepository<Trade, Long> {
	Trade findById(Integer id);
}
