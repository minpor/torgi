package ru.altimeta.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.altimeta.domain.Chat;
import ru.altimeta.domain.Conversation;

import java.util.List;

public interface ChatDao extends JpaRepository<Chat, Long>{
	List<Chat> findByConversation(Conversation conversation);
}
