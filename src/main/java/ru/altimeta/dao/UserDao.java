package ru.altimeta.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.altimeta.domain.User;

public interface UserDao extends JpaRepository<User, Long> {
	User findById(Integer id);
}
