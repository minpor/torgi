package ru.altimeta.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoungException extends Exception {
	public NotFoungException(String message) {
		super(message);
	}
}
