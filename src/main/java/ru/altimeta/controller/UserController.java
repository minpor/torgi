package ru.altimeta.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.altimeta.domain.Chat;
import ru.altimeta.domain.Conversation;
import ru.altimeta.domain.enums.SubjectType;
import ru.altimeta.dto.CreateConversationDto;
import ru.altimeta.dto.MessageDto;
import ru.altimeta.dto.UserDto;
import ru.altimeta.exception.NotFoungException;
import ru.altimeta.service.UserService;

import java.util.List;

@RestController
@RequestMapping(value = "/torgi")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "subject_type", method = RequestMethod.GET)
	public List<SubjectType> getSubjectTypes() {
		return userService.getSubjectTypes();
	}

	@RequestMapping(value = "subject_type/{subjectType}/subject_id/{subjectId}/users", method = RequestMethod.GET)
	public List<UserDto> getUserBySubjectType(@PathVariable int subjectType, @PathVariable Integer subjectId) throws NotFoungException {
		return userService.getUserBySubjectType(subjectType, subjectId);
	}

	@RequestMapping(value = "conversation", method = RequestMethod.POST)
	public void createConversation(@RequestBody CreateConversationDto dto) {
		userService.createConversation(dto);
	}

	@RequestMapping(value = "conversations", method = RequestMethod.GET)
	public List<Conversation> getConversations() {
		return userService.getConversations();
	}

	@RequestMapping(value = "conversation/{conversationId}/messages", method = RequestMethod.GET)
	public List<Chat> getMessages(@PathVariable long conversationId) throws NotFoungException {
		return userService.getMessages(conversationId);
	}

	@RequestMapping(value = "conversation/message", method = RequestMethod.POST)
	public void createMessage(@RequestBody MessageDto dto) throws NotFoungException {
		userService.createMessage(dto);
	}


}
