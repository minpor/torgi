package ru.altimeta.dto;

import java.util.Arrays;

public class CreateConversationDto {
	private int subjectType;
	private int[] userId;

	public int getSubjectType() {
		return subjectType;
	}

	public void setSubjectType(int subjectType) {
		this.subjectType = subjectType;
	}

	public int[] getUserId() {
		return userId;
	}

	public void setUserId(int[] userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "CreateConversationDto{" +
				"subjectType=" + subjectType +
				", userId=" + Arrays.toString(userId) +
				'}';
	}
}
