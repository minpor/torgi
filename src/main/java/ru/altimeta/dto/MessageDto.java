package ru.altimeta.dto;

public class MessageDto {
	private long conversationId;
	private int userId;
	private String message;

	public long getConversationId() {
		return conversationId;
	}

	public void setConversationId(long conversationId) {
		this.conversationId = conversationId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "MessageDto{" +
				"conversationId=" + conversationId +
				", userId=" + userId +
				", message='" + message + '\'' +
				'}';
	}
}
