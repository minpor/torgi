package ru.altimeta.dto;

import ru.altimeta.domain.User;


public class UserDto {
	private int id;
	private String login;
	private String name;

	public UserDto(User user) {
		this.id = user.getId();
		this.login = user.getLogin();
		this.name = user.getName();
	}

	public int getId() {
		return id;
	}

	public String getLogin() {
		return login;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "UserDto{" +
				"id=" + id +
				", login='" + login + '\'' +
				", name='" + name + '\'' +
				'}';
	}
}