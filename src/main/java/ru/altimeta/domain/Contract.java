package ru.altimeta.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Contract implements Serializable {
	private static final long serialVersionUID = -8619696261830519642L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private int id;

	@OneToMany
	private List<User> owners;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<User> getOwners() {
		return owners;
	}

	public void setOwners(List<User> owners) {
		this.owners = owners;
	}
}
