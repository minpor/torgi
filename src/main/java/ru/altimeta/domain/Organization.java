package ru.altimeta.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Organization implements Serializable {
	private static final long serialVersionUID = -2134637839136804018L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private int id;

	@Column(name = "name")
	private String name;

	@OneToMany
	private List<User> members;

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public List<User> getMembers() {
		return members;
	}
}
