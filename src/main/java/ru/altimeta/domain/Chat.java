package ru.altimeta.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Chat implements Serializable {
	private static final long serialVersionUID = -1994538924736209695L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private int id;

	@ManyToOne
	private Conversation conversation;

	@Column
	private String message;

	@ManyToOne
	private User user;

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
