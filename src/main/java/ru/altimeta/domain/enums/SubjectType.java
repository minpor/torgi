package ru.altimeta.domain.enums;

public enum SubjectType {
	/** Без субъекта */
	NON_SUBJECT(1),
	/** Субъект торги */
	TRADE(2),
	/** Субъект контракт */
	CONTRACT(3);

	private int value;

	SubjectType(int i) {
		this.value = i;
	}

	public int getValue() {
		return value;
	}
}
