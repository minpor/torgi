package ru.altimeta.domain;

import ru.altimeta.domain.enums.SubjectType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Conversation implements Serializable {
	private static final long serialVersionUID = -8391334007563502291L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private long id;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "conversation_member", joinColumns = {
			@JoinColumn(name = "conversation_id", nullable = false, updatable = false)
	}, inverseJoinColumns = {@JoinColumn(name = "user_id")})
	private Set<User> user = new HashSet<>();


	@Column(name = "type", columnDefinition = "varchar(15)")
	@Enumerated(EnumType.STRING)
	private SubjectType type;

	public long getId() {
		return id;
	}

	public Set<User> getUser() {
		return user;
	}

	public void setUser(Set<User> user) {
		this.user = user;
	}

	public SubjectType getType() {
		return type;
	}

	public void setType(SubjectType type) {
		this.type = type;
	}
}
